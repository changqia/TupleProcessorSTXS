#include <TupleProcessorSTXS/AnalysisProcessor_STXS.h>
#include <TupleProcessorSTXS/AnalysisProcessor_STXS_adv.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class AnalysisProcessor_STXS+;
#pragma link C++ class AnalysisProcessor_STXS_adv+;
#endif

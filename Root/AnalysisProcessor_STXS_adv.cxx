#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <TupleProcessorSTXS/AnalysisProcessor_STXS_adv.h>

#include <AsgTools/MessageCheck.h>
#include <cmath>

#define length(array) (sizeof(array) / sizeof(*(array)))

// this is needed to distribute the algorithm to the workers
ClassImp(AnalysisProcessor_STXS_adv)



AnalysisProcessor_STXS_adv :: AnalysisProcessor_STXS_adv ():
m_config(nullptr),
m_histSvc(nullptr),
m_histNameSvc(nullptr),
m_xSectionProvider(nullptr),
m_reader(nullptr),
m_Base(-1),
m_debug(false)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode AnalysisProcessor_STXS_adv :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisProcessor_STXS_adv :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  Info("AnalysisProcessor_STXS_adv()", "Initializing histograms.");
  // histogram manager
  m_histSvc     = new HistSvc();
  m_histNameSvc = new HistNameSvc();
  m_histSvc->SetNameSvc(m_histNameSvc);
  bool fillHists = true;
  m_histSvc->SetFillHists(fillHists);
  Info("histInitialize()", "Initializing histograms finished!");
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisProcessor_STXS_adv :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisProcessor_STXS_adv :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  Info("AnalysisProcessor_STXS_adv()", "Initializing tree!");
  Init (wk()->tree());
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisProcessor_STXS_adv :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  ANA_CHECK_SET_TYPE (EL::StatusCode);
  Info("AnalysisProcessor_STXS_adv()", "Initialize ()");
  m_config->getif<bool>("debug", m_debug);
  //m_config->getif<bool>("STXS_Stage1_Base", m_Base);
  m_config->getif<int>("STXS_Stage1_Base", m_Base);

  //Xsec Provider
  //------------------
  std::string xSectionFile = gSystem->Getenv("ROOTCOREBIN");
  xSectionFile      += "/data/FrameworkSub/XSections_13TeV.txt";
  // if xSectionFile is given in config file, replace all we just did
  m_config->getif<string>("xSectionFile", xSectionFile);
  m_xSectionProvider = new XSectionProvider(xSectionFile);

  if (!m_xSectionProvider) {
    Error("initializeTools()", "XSection provider not initialized!");
    return EL::StatusCode::FAILURE;
  }

  m_eventCounter = 0;
  m_eventWithNan = 0;
  m_reader = new AnalysisReader_VHbb;
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisProcessor_STXS_adv :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  //if (m_debug) Info("AnalysisProcessor_STXS_adv()", "execute () tree()->GetEntry");
  wk()->tree()->GetEntry (wk()->treeEntry());
  //if (m_debug) Info("AnalysisProcessor_STXS_adv()", "execute () tree()->GetEntry successfully!");

  //m_histNameSvc->set_sample("sample");
  //if (m_debug) Info("AnalysisProcessor_STXS_adv()", "execute () set_sample = %i",mcChannel);
  TString SN = m_xSectionProvider->getSampleName(mcChannel);
  if (SN == "qqWlvH125") {
    if (mcChannel == 345053) SN+="M";
    else if (mcChannel == 345054) SN +="P";
  }
  //m_histNameSvc->set_sample(m_xSectionProvider->getSampleName(mcChannel));
  m_histNameSvc->set_sample(SN.Data());

  if( (m_eventCounter % 10000) ==0 ) Info("execute()", "Event number = %li", m_eventCounter );
  m_eventCounter++;

  // get the truth bin
  //if (m_debug) Info("execute()", "Truth Bin number is '%d'.", HTXS_Stage1_Category_pTjet30 );

  std::string stage1_name = "";
  if ( m_Base < 0 ) {
    Warning("AnalysisReader_VHbb::TranslateStage1Bin()", "Unknown m_Base!! Exit!");
    exit(1);
  } else if ( m_Base < 2) {
    stage1_name = m_reader->TranslateStage1Bin (HTXS_Stage1_Category_pTjet30, mcChannel, m_Base, HTXS_V_pt, HTXS_Njets_pTjet30);
  } else {
    stage1_name = m_reader->TranslateStage1BinAdv (HTXS_Stage1_Category_pTjet30, mcChannel, m_Base, HTXS_V_pt, HTXS_Njets_pTjet30);
  }

  if (m_debug) {
    //if (EventNumber != 1783463) return EL::StatusCode::SUCCESS;
    if (EventNumber != 301957) return EL::StatusCode::SUCCESS;
    //Info("execute()", "Truth Bin number is '%d'.", HTXS_Stage1_Category_pTjet30 );
    Info("execute()", "Truth Bin  is '%s'.", stage1_name.c_str() );
    Info("execute ()", "EW correction         : %.6f", VHEW);
    Info("execute ()", "HiggsWeight weight0   : %.6f", weight);
    Info("execute ()", "HiggsWeight   PDF0    : %.6f", PDFWeight0);
    Info("execute ()", "HiggsWeight nominal   : %.6f", NominalWeight);
    Info("execute ()", "HiggsWeight alphaS_up : %.6f", alphaS_up);
    Info("execute ()", "HiggsWeight alphaS_dn : %.6f", alphaS_dn);
    int NPDF = PDFweights->size();
    for( int ipdf(0); ipdf < NPDF; ipdf++)  {
      Info("execute ()", "HiggsWeight  %2i  PDF  : %.6f", ipdf+1, PDFweights->at(ipdf));
    }
    int NQCD = QCDweights->size();
    for (int iqcd(0); iqcd < NQCD; iqcd++)  {
      Info("execute ()", "HiggsWeight  %2i  QCD  : %.6f", iqcd+1, QCDweights->at(iqcd));
    }
  }

  // fill the weight0
  FillStage1AndPTV( stage1_name, weight, VHEW, "Weight0" );
  /*
  fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight*VHEW,"_Weight0");
  fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight*VHEW,"_Weight0",false,HTXS_V_pt);
	m_histSvc->BookFillHist(stage1_name+"_Weight0_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight*VHEW);
	m_histSvc->BookFillHist("Weight0_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight*VHEW); // inclusive

  // fill the weight0 without EW
  fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight,"_Weight0NOEW");
  fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight,"_Weight0NOEW",false,HTXS_V_pt);
	m_histSvc->BookFillHist(stage1_name+"_Weight0NOEW_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight);
	m_histSvc->BookFillHist("Weight0NOEW_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight); // inclusive
  */

  // Check if the variation has a Nan, need to be investigated!
  bool hasNan = false;
  if (std::isnan(weight)) {Error("execute()", "weight is nan! For Event number %d", EventNumber ); hasNan = true;}
  if (std::isnan(NominalWeight)) {Error("execute()", "NominalWeight is nan! For Event number %d", EventNumber );  hasNan = true;}
  if (std::isnan(alphaS_up)) {Error("execute()", "alphaS_up is nan! For Event number %d", EventNumber);  hasNan = true;}
  if (std::isnan(alphaS_dn)) {Error("execute()", "alphaS_dn is nan! For Event number %d", EventNumber);  hasNan = true;}
  for (int ipdf(0); ipdf< PDFweights->size(); ipdf++) {
    if (std::isnan(PDFweights->at(ipdf))) {Error("execute()", "PDF No.%d is a nan! For Event number %d", ipdf, EventNumber);  hasNan = true;}
  }
  for (int iqcd(0); iqcd < QCDweights->size(); iqcd++)  {
    if (std::isnan(QCDweights->at(iqcd))) {Error("execute()", "QCD No.%d is a nan! For Event number %d", iqcd, EventNumber);  hasNan = true;}
  }
  if (hasNan) {
    m_eventWithNan++;
    //return EL::StatusCode::SUCCESS;
  }
  
  /*
  // fill the nominal
  fillStage1Bin(HTXS_Stage1_Category_pTjet30,NominalWeight*VHEW,"_Nominal");
  fillStage1Bin(HTXS_Stage1_Category_pTjet30,NominalWeight*VHEW,"_Nominal",false,HTXS_V_pt);
	m_histSvc->BookFillHist(stage1_name+"_Nominal_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., NominalWeight*VHEW);
	m_histSvc->BookFillHist("Nominal_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., NominalWeight*VHEW); // inclusive

  // fill the nominal without EW
  fillStage1Bin(HTXS_Stage1_Category_pTjet30,NominalWeight,"_NominalNOEW");
  fillStage1Bin(HTXS_Stage1_Category_pTjet30,NominalWeight,"_NominalNOEW",false,HTXS_V_pt);
	m_histSvc->BookFillHist(stage1_name+"_NominalNOEW_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., NominalWeight);
	m_histSvc->BookFillHist("NominalNOEW_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., NominalWeight); // inclusive
  */
  FillStage1AndPTV( stage1_name, NominalWeight, VHEW, "Nominal" );

  // alphaSup
  //float weight_alphaSup_var = weight * alphaS_up / NominalWeight;
  float weight_alphaSup_var = alphaS_up;
  FillStage1AndPTV( stage1_name, weight_alphaSup_var, VHEW, "alphaSup" );
  /*
  fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight_alphaSup_var*VHEW,"_alphaSup");
  fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight_alphaSup_var*VHEW,"_alphaSup",false,HTXS_V_pt);
	m_histSvc->BookFillHist(stage1_name+"_alphaSup_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight_alphaSup_var*VHEW);
	m_histSvc->BookFillHist("alphaSup_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight_alphaSup_var*VHEW); // inclusive

  fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight_alphaSup_var,"_alphaSupNOEW");
  fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight_alphaSup_var,"_alphaSupNOEW",false,HTXS_V_pt);
	m_histSvc->BookFillHist(stage1_name+"_alphaSupNOEW_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight_alphaSup_var);
	m_histSvc->BookFillHist("alphaSupNOEW_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight_alphaSup_var); // inclusive
  */

  // alphaSdn
  //float weight_alphaSdn_var = weight * alphaS_dn / NominalWeight;
  float weight_alphaSdn_var = alphaS_dn;
  FillStage1AndPTV( stage1_name, weight_alphaSdn_var, VHEW, "alphaSdn" );
  /*
  fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight_alphaSdn_var*VHEW,"_alphaSdn");
  fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight_alphaSdn_var*VHEW,"_alphaSdn",false,HTXS_V_pt);
	m_histSvc->BookFillHist(stage1_name+"_alphaSdn_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight_alphaSdn_var*VHEW);
	m_histSvc->BookFillHist("alphaSdn_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight_alphaSdn_var*VHEW); // inclusive

  fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight_alphaSdn_var,"_alphaSdnNOEW");
  fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight_alphaSdn_var,"_alphaSdnNOEW",false,HTXS_V_pt);
	m_histSvc->BookFillHist(stage1_name+"_alphaSdnNOEW_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight_alphaSdn_var);
	m_histSvc->BookFillHist("alphaSdnNOEW_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight_alphaSdn_var); // inclusive
  */

  // PDF
  for (int ipdf(0); ipdf< PDFweights->size(); ipdf++) {
    //float weight_pdf_var = weight * PDFweights->at(ipdf) / NominalWeight;
    float weight_pdf_var = PDFweights->at(ipdf);
    TString suffixPDF = TString::Format("PDF%d",ipdf+1); // PDF0 has other meaning, it is nominal
    FillStage1AndPTV( stage1_name, weight_pdf_var, VHEW, suffixPDF.Data() );
    /*
    fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight_pdf_var*VHEW,"_"+suffixPDF);
    fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight_pdf_var*VHEW,"_"+suffixPDF,false,HTXS_V_pt);
	  m_histSvc->BookFillHist(stage1_name+"_"+suffixPDF.Data()+"_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight_pdf_var*VHEW);
	  m_histSvc->BookFillHist(string(suffixPDF.Data())+"_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight_pdf_var*VHEW); // inclusive

    fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight_pdf_var,"_"+suffixPDF+"NOEW");
    fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight_pdf_var,"_"+suffixPDF+"NOEW",false,HTXS_V_pt);
    m_histSvc->BookFillHist(stage1_name+"_"+suffixPDF.Data()+"NOEW_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight_pdf_var);
	  m_histSvc->BookFillHist(string(suffixPDF.Data())+"NOEW_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight_pdf_var); // inclusive
    */
  }

  // QCD
  for (int iqcd(0); iqcd < QCDweights->size(); iqcd++)  {
    //float weight_qcd_var = weight * QCDweights->at(iqcd) / NominalWeight;
    //float weight_qcd_var = QCDweights->at(iqcd)*weight/NominalWeight; 
    // Since Stage13PP, this is fixed
    float weight_qcd_var = QCDweights->at(iqcd); 
    // inside TruthWeightTools/Root/HiggsWeightTool.cxx for (auto idx:m_qcd) hw.qcd.push_back(getWeight(weights,idx)*w_nom/hw.weight0); 
    TString suffixQCD = TString::Format("QCD%d",iqcd+1);
    FillStage1AndPTV( stage1_name, weight_qcd_var, VHEW, suffixQCD.Data() );
    /*
    fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight_qcd_var*VHEW,"_"+suffixQCD);
    fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight_qcd_var*VHEW,"_"+suffixQCD,false,HTXS_V_pt);
	  m_histSvc->BookFillHist(stage1_name+"_"+suffixQCD.Data()+"_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight_qcd_var*VHEW);
	  m_histSvc->BookFillHist(string(suffixQCD.Data())+"_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight_qcd_var*VHEW); // inclusive

    fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight_qcd_var,"_"+suffixQCD+"NOEW");
    fillStage1Bin(HTXS_Stage1_Category_pTjet30,weight_qcd_var,"_"+suffixQCD+"NOEW",false,HTXS_V_pt);
	  m_histSvc->BookFillHist(stage1_name+"_"+suffixQCD.Data()+"NOEW_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight_qcd_var);
	  m_histSvc->BookFillHist(string(suffixQCD.Data())+"NOEW_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., weight_qcd_var); // inclusive
    */
  }

  return EL::StatusCode::SUCCESS;

}



EL::StatusCode AnalysisProcessor_STXS_adv :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisProcessor_STXS_adv :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  Info("finalize()", "Processed events                 = %li", m_eventCounter);
  Info("finalize()", "Skipped events due to Nan Weight = %li", m_eventWithNan);
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisProcessor_STXS_adv :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  m_histSvc->Write(wk());
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisProcessor_STXS_adv::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).


   // Set object pointer
   PDFweights = 0;
   QCDweights = 0;

   // Set branch addresses and branch pointers
   Info("AnalysisProcessor_STXS_adv()", "Init");
   if (!tree) return EL::StatusCode::FAILURE;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("mcChannel", &mcChannel, &b_mcChannel);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("weight", &weight, &b_weight);
   fChain->SetBranchAddress("PDFweights", &PDFweights, &b_PDFweights);
   fChain->SetBranchAddress("QCDweights", &QCDweights, &b_QCDweights);
   fChain->SetBranchAddress("NominalWeight", &NominalWeight, &b_NominalWeight);
   fChain->SetBranchAddress("Weight0", &Weight0, &b_Weight0);
   fChain->SetBranchAddress("PDFWeight0", &PDFWeight0, &b_PDFWeight0);
   fChain->SetBranchAddress("alphaS_up", &alphaS_up, &b_alphaS_up);
   fChain->SetBranchAddress("alphaS_dn", &alphaS_dn, &b_alphaS_dn);
   fChain->SetBranchAddress("VHEW", &VHEW, &b_VHEW);
   fChain->SetBranchAddress("HTXS_Higgs_pt", &HTXS_Higgs_pt, &b_HTXS_Higgs_pt);
   fChain->SetBranchAddress("HTXS_Higgs_eta", &HTXS_Higgs_eta, &b_HTXS_Higgs_eta);
   fChain->SetBranchAddress("HTXS_Higgs_phi", &HTXS_Higgs_phi, &b_HTXS_Higgs_phi);
   fChain->SetBranchAddress("HTXS_Higgs_m", &HTXS_Higgs_m, &b_HTXS_Higgs_m);
   fChain->SetBranchAddress("HTXS_V_pt", &HTXS_V_pt, &b_HTXS_V_pt);
   fChain->SetBranchAddress("HTXS_V_eta", &HTXS_V_eta, &b_HTXS_V_eta);
   fChain->SetBranchAddress("HTXS_V_phi", &HTXS_V_phi, &b_HTXS_V_phi);
   fChain->SetBranchAddress("HTXS_V_m", &HTXS_V_m, &b_HTXS_V_m);
   fChain->SetBranchAddress("HTXS_prodMode", &HTXS_prodMode, &b_HTXS_prodMode);
   fChain->SetBranchAddress("HTXS_Stage0_Category", &HTXS_Stage0_Category, &b_HTXS_Stage0_Category);
   fChain->SetBranchAddress("HTXS_Stage1_Category_pTjet25", &HTXS_Stage1_Category_pTjet25, &b_HTXS_Stage1_Category_pTjet25);
   fChain->SetBranchAddress("HTXS_Stage1_Category_pTjet30", &HTXS_Stage1_Category_pTjet30, &b_HTXS_Stage1_Category_pTjet30);
   fChain->SetBranchAddress("HTXS_Njets_pTjet25", &HTXS_Njets_pTjet25, &b_HTXS_Njets_pTjet25);
   fChain->SetBranchAddress("HTXS_Njets_pTjet30", &HTXS_Njets_pTjet30, &b_HTXS_Njets_pTjet30);

   return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisProcessor_STXS_adv :: fillStage1Bin ( int stage1, float weight_inserted, TString remark, int STXS_Stage1_Base, float ptV )
{

  float local_weight = weight_inserted;
  string sample_name = m_histNameSvc->get_sample();

  static std::string Stage1Names_Nicety[82] = {
  // qq -> ZH NUNU 16
  "QQ2HNUNU_FWDH",
  "QQ2HNUNU_PTV_0_75_0J",     "QQ2HNUNU_PTV_0_75_1J",      "QQ2HNUNU_PTV_0_75_GE2J",
  "QQ2HNUNU_PTV_75_150_0J",   "QQ2HNUNU_PTV_75_150_1J",    "QQ2HNUNU_PTV_75_150_GE2J",
  "QQ2HNUNU_PTV_150_250_0J",  "QQ2HNUNU_PTV_150_250_1J",   "QQ2HNUNU_PTV_150_250_GE2J",
  "QQ2HNUNU_PTV_250_400_0J",  "QQ2HNUNU_PTV_250_400_1J",   "QQ2HNUNU_PTV_250_400_GE2J",
  "QQ2HNUNU_PTV_GT400_0J",    "QQ2HNUNU_PTV_GT400_1J",     "QQ2HNUNU_PTV_GT400_GE2J",
  // gg -> ZH NUNU 16                                                                        
  "GG2HNUNU_FWDH",                                                                           
  "GG2HNUNU_PTV_0_75_0J",     "GG2HNUNU_PTV_0_75_1J",      "GG2HNUNU_PTV_0_75_GE2J",
  "GG2HNUNU_PTV_75_150_0J",   "GG2HNUNU_PTV_75_150_1J",    "GG2HNUNU_PTV_75_150_GE2J",
  "GG2HNUNU_PTV_150_250_0J",  "GG2HNUNU_PTV_150_250_1J",   "GG2HNUNU_PTV_150_250_GE2J",
  "GG2HNUNU_PTV_250_400_0J",  "GG2HNUNU_PTV_250_400_1J",   "GG2HNUNU_PTV_250_400_GE2J",
  "GG2HNUNU_PTV_GT400_0J",    "GG2HNUNU_PTV_GT400_1J",     "GG2HNUNU_PTV_GT400_GE2J",
  // qq -> WH 16                                                                              
  "QQ2HLNU_FWDH",                                                                            
  "QQ2HLNU_PTV_0_75_0J",      "QQ2HLNU_PTV_0_75_1J",       "QQ2HLNU_PTV_0_75_GE2J",
  "QQ2HLNU_PTV_75_150_0J",    "QQ2HLNU_PTV_75_150_1J",     "QQ2HLNU_PTV_75_150_GE2J",
  "QQ2HLNU_PTV_150_250_0J",   "QQ2HLNU_PTV_150_250_1J",    "QQ2HLNU_PTV_150_250_GE2J",
  "QQ2HLNU_PTV_250_400_0J",   "QQ2HLNU_PTV_250_400_1J",    "QQ2HLNU_PTV_250_400_GE2J",
  "QQ2HLNU_PTV_GT400_0J",     "QQ2HLNU_PTV_GT400_1J",      "QQ2HLNU_PTV_GT400_GE2J",
  // qq -> ZH LL 16                                                                           
  "QQ2HLL_FWDH",                                                                             
  "QQ2HLL_PTV_0_75_0J",       "QQ2HLL_PTV_0_75_1J",        "QQ2HLL_PTV_0_75_GE2J",
  "QQ2HLL_PTV_75_150_0J",     "QQ2HLL_PTV_75_150_1J",      "QQ2HLL_PTV_75_150_GE2J",
  "QQ2HLL_PTV_150_250_0J",    "QQ2HLL_PTV_150_250_1J",     "QQ2HLL_PTV_150_250_GE2J",
  "QQ2HLL_PTV_250_400_0J",    "QQ2HLL_PTV_250_400_1J",     "QQ2HLL_PTV_250_400_GE2J",
  "QQ2HLL_PTV_GT400_0J",      "QQ2HLL_PTV_GT400_1J",       "QQ2HLL_PTV_GT400_GE2J",
  // gg -> ZH LL 16                                                                           
  "GG2HLL_FWDH",                                                                             
  "GG2HLL_PTV_0_75_0J",       "GG2HLL_PTV_0_75_1J",        "GG2HLL_PTV_0_75_GE2J",
  "GG2HLL_PTV_75_150_0J",     "GG2HLL_PTV_75_150_1J",      "GG2HLL_PTV_75_150_GE2J",
  "GG2HLL_PTV_150_250_0J",    "GG2HLL_PTV_150_250_1J",     "GG2HLL_PTV_150_250_GE2J",
  "GG2HLL_PTV_250_400_0J",    "GG2HLL_PTV_250_400_1J",     "GG2HLL_PTV_250_400_GE2J",
  "GG2HLL_PTV_GT400_0J",      "GG2HLL_PTV_GT400_1J",       "GG2HLL_PTV_GT400_GE2J",
  // unknown
  "UNKNOWN",
  // total, always filled
  "TOTAL",
  };

  static std::string Stage1Names_Fine[37] = {
  // qq -> ZH NUNU 7
  "QQ2HNUNU_FWDH","QQ2HNUNU_PTV_0_75","QQ2HNUNU_PTV_75_150",
  "QQ2HNUNU_PTV_150_250_0J","QQ2HNUNU_PTV_150_250_GE1J","QQ2HNUNU_PTV_GT250_0J","QQ2HNUNU_PTV_GT250_GE1J",
  // gg -> ZH NUNU 7
  "GG2HNUNU_FWDH","GG2HNUNU_PTV_0_75","GG2HNUNU_PTV_75_150",
  "GG2HNUNU_PTV_150_250_0J","GG2HNUNU_PTV_150_250_GE1J","GG2HNUNU_PTV_GT250_0J","GG2HNUNU_PTV_GT250_GE1J",
  // qq -> WH 7
  "QQ2HLNU_FWDH","QQ2HLNU_PTV_0_75","QQ2HLNU_PTV_75_150",
  "QQ2HLNU_PTV_150_250_0J","QQ2HLNU_PTV_150_250_GE1J","QQ2HLNU_PTV_GT250_0J","QQ2HLNU_PTV_GT250_GE1J",
  // qq -> ZH LL 7
  "QQ2HLL_FWDH","QQ2HLL_PTV_0_75","QQ2HLL_PTV_75_150",
  "QQ2HLL_PTV_150_250_0J","QQ2HLL_PTV_150_250_GE1J","QQ2HLL_PTV_GT250_0J","QQ2HLL_PTV_GT250_GE1J",
  // gg -> ZH LL 7
  "GG2HLL_FWDH","GG2HLL_PTV_0_75","GG2HLL_PTV_75_150",
  "GG2HLL_PTV_150_250_0J","GG2HLL_PTV_150_250_GE1J","GG2HLL_PTV_GT250_0J","GG2HLL_PTV_GT250_GE1J",
  // unknown
  "UNKNOWN",
  // total, always filled
  "TOTAL",
  };

  // stage1 category
  static std::string Stage1Names_Base[25] = {
  // qq -> ZH
  "QQ2HNUNU_FWDH","QQ2HNUNU_PTV_0_150","QQ2HNUNU_PTV_150_250_0J","QQ2HNUNU_PTV_150_250_GE1J","QQ2HNUNU_PTV_GT250",
  // gg -> ZH
  "GG2HNUNU_FWDH","GG2HNUNU_PTV_0_150","GG2HNUNU_PTV_GT150_0J","GG2HNUNU_PTV_GT150_GE1J",
  // qq -> WH
  "QQ2HLNU_FWDH","QQ2HLNU_PTV_0_150","QQ2HLNU_PTV_150_250_0J","QQ2HLNU_PTV_150_250_GE1J","QQ2HLNU_PTV_GT250",
  // qq -> ZH
  "QQ2HLL_FWDH","QQ2HLL_PTV_0_150","QQ2HLL_PTV_150_250_0J","QQ2HLL_PTV_150_250_GE1J","QQ2HLL_PTV_GT250",
  // gg -> ZH
  "GG2HLL_FWDH","GG2HLL_PTV_0_150","GG2HLL_PTV_GT150_0J","GG2HLL_PTV_GT150_GE1J",
  // unknown
  "UNKNOWN",
  // total, always filled
  "TOTAL",
  };

  // std::string stage1_name = reader.TranslateStage1Bin(HTXS_Stage1_Category_pTjet30);
  std::string stage1_name = "";
  if ( STXS_Stage1_Base < 0 ) {
    Warning("AnalysisProcessor_STXS_adv::fillStage1Bin()", "Unknown STXS_Stage1_Base!! Exit!");
    exit(1);
  } else if ( STXS_Stage1_Base < 2) {
    stage1_name = m_reader->TranslateStage1Bin (stage1, mcChannel, m_Base, HTXS_V_pt, HTXS_Njets_pTjet30);
  } else {
    stage1_name = m_reader->TranslateStage1BinAdv (stage1, mcChannel, m_Base, HTXS_V_pt, HTXS_Njets_pTjet30);
  }

  string fillname = sample_name+remark.Data()+"_Stage1";

  if ( STXS_Stage1_Base == 0 ) {
    // for a certain bin
    m_histSvc->BookFillCutHist(fillname+"Base", length(Stage1Names_Base), Stage1Names_Base, stage1_name, local_weight);
    m_histSvc->BookFillCutHist(fillname+"Base", length(Stage1Names_Base), Stage1Names_Base, "TOTAL",     local_weight);

    // total
    m_histSvc->BookFillCutHist(fillname+"Base"+"_NoWeight", length(Stage1Names_Base), Stage1Names_Base, stage1_name, 1.);
    m_histSvc->BookFillCutHist(fillname+"Base"+"_NoWeight", length(Stage1Names_Base), Stage1Names_Base, "TOTAL",     1.);
  } else if ( STXS_Stage1_Base == 1 ){

    // for a certain bin
    m_histSvc->BookFillCutHist(fillname+"Fine", length(Stage1Names_Fine), Stage1Names_Fine, stage1_name, local_weight);
    m_histSvc->BookFillCutHist(fillname+"Fine", length(Stage1Names_Fine), Stage1Names_Fine, "TOTAL",     local_weight);

    // total
    m_histSvc->BookFillCutHist(fillname+"Fine"+"_NoWeight", length(Stage1Names_Fine), Stage1Names_Fine, stage1_name, 1.);
    m_histSvc->BookFillCutHist(fillname+"Fine"+"_NoWeight", length(Stage1Names_Fine), Stage1Names_Fine, "TOTAL",     1.);

  } else if ( STXS_Stage1_Base == 2){ 

    // for a certain bin
    m_histSvc->BookFillCutHist(fillname+"Nicest", length(Stage1Names_Nicety), Stage1Names_Nicety, stage1_name, local_weight);
    m_histSvc->BookFillCutHist(fillname+"Nicest", length(Stage1Names_Nicety), Stage1Names_Nicety, "TOTAL",     local_weight);

    // total
    m_histSvc->BookFillCutHist(fillname+"Nicest"+"_NoWeight", length(Stage1Names_Nicety), Stage1Names_Nicety, stage1_name, 1.);
    m_histSvc->BookFillCutHist(fillname+"Nicest"+"_NoWeight", length(Stage1Names_Nicety), Stage1Names_Nicety, "TOTAL",     1.);

  }

  return EL::StatusCode::SUCCESS;

}

EL::StatusCode AnalysisProcessor_STXS_adv :: FillStage1AndPTV(std::string stage1_name, float Raw_weight, float EWCorrection, std::string weight_info){

  float Fillweight = 1.0;
  /*
  if ( Raw_weight > 50. ) Fillweight = 50.;
  else if ( Raw_weight < -50. ) Fillweight = -50.;
  else Fillweight = Raw_weight;
  */
  Fillweight = Raw_weight;

  fillStage1Bin(HTXS_Stage1_Category_pTjet30,Fillweight*EWCorrection,"_"+weight_info,0);
  fillStage1Bin(HTXS_Stage1_Category_pTjet30,Fillweight*EWCorrection,"_"+weight_info,1,HTXS_V_pt);
  fillStage1Bin(HTXS_Stage1_Category_pTjet30,Fillweight*EWCorrection,"_"+weight_info,2,HTXS_V_pt);
	m_histSvc->BookFillHist(stage1_name+"_"+weight_info+"_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., Fillweight*EWCorrection);
	m_histSvc->BookFillHist(stage1_name+"_"+weight_info+"_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., Fillweight*EWCorrection);
	m_histSvc->BookFillHist(weight_info+"_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., Fillweight*EWCorrection); // inclusive

  fillStage1Bin(HTXS_Stage1_Category_pTjet30,Fillweight,"_"+weight_info+"NOEW",0);
  fillStage1Bin(HTXS_Stage1_Category_pTjet30,Fillweight,"_"+weight_info+"NOEW",1,HTXS_V_pt);
  fillStage1Bin(HTXS_Stage1_Category_pTjet30,Fillweight,"_"+weight_info+"NOEW",2,HTXS_V_pt);
	m_histSvc->BookFillHist(stage1_name+"_"+weight_info+"NOEW"+"_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., Fillweight);
	m_histSvc->BookFillHist(stage1_name+"_"+weight_info+"NOEW"+"_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., Fillweight);
	m_histSvc->BookFillHist(weight_info+"NOEW"+"_TruthPTV", 1000, 0., 1000., HTXS_V_pt/1000., Fillweight); // inclusive

  return EL::StatusCode::SUCCESS;

}

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <TupleProcessorSTXS/AnalysisProcessor_STXS.h>

#include <AsgTools/MessageCheck.h>


// this is needed to distribute the algorithm to the workers
ClassImp(AnalysisProcessor_STXS)



AnalysisProcessor_STXS :: AnalysisProcessor_STXS ():
m_config(nullptr),
m_histSvc(nullptr),
m_histNameSvc(nullptr),
m_reader(nullptr),
m_Base(0),
m_debug(false)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode AnalysisProcessor_STXS :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisProcessor_STXS :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  Info("histInitialize()", "Initializing histograms.");
  // histogram manager
  m_histSvc     = new HistSvc();
  m_histNameSvc = new HistNameSvc();
  m_histSvc->SetNameSvc(m_histNameSvc);
  bool fillHists = true;
  m_histSvc->SetFillHists(fillHists);
  Info("histInitialize()", "Initializing histograms finished!");
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisProcessor_STXS :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisProcessor_STXS :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  Init (wk()->tree());
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisProcessor_STXS :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  ANA_CHECK_SET_TYPE (EL::StatusCode);
  m_config->getif<bool>("debug", m_debug);
  m_config->getif<int>("STXS_Stage1_Base", m_Base);

  m_eventCounter = 0;
  m_reader = new AnalysisReader_VHbb;
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisProcessor_STXS :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  wk()->tree()->GetEntry (wk()->treeEntry());
  //m_histNameSvc->set_sample("sample");
  if( (m_eventCounter % 100) ==0 ) Info("execute()", "Event number = %i", m_eventCounter );
  m_eventCounter++;

  // get the truth bin
  if (m_debug) Info("execute()", "Truth Bin number is '%d'.", HTXS_Stage1_Category_pTjet30 );

  // set DSID to 0 to avoid NUNU separated from LL, because this is 2 lepton
  //std::string stage1_name = m_reader->TranslateStage1Bin(HTXS_Stage1_Category_pTjet30,0,m_Base,HTXS_V_pt,HTXS_Njets_pTjet30);
  std::string stage1_name = m_reader->TranslateStage1BinAdv(HTXS_Stage1_Category_pTjet30,0,m_Base,HTXS_V_pt,HTXS_Njets_pTjet30);
  if (m_debug) Info("execute()", "Truth Bin  is '%s'.", stage1_name.c_str() );

  //std::string category_name = TranslateRecoL2(category->at(0));
  std::string category_name = TranslateRecoL2( category_STXS->at(0) );
  //std::string category_name = "temp";
  //if (m_debug) Info("execute()", "Recon Cate size is '%i'.", category->size() );
  if (m_debug) Info("execute()", "Recon Cate name is '%s'.", category_name.c_str() );

  m_reader->fillStage1Bin_RecoCategory_EPS ( m_histSvc, "qqZllH125", stage1_name, category_name, weight, m_Base );

  /*
  // get the category
  category;
 
  // event weight
  weight;
  */


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisProcessor_STXS :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisProcessor_STXS :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode AnalysisProcessor_STXS :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  m_histSvc->Write(wk());
  return EL::StatusCode::SUCCESS;
}

std::string AnalysisProcessor_STXS :: TranslateRecoL2 (int cate)
{
     
   std::string cate_name = "";
   if(cate == 52202)      cate_name = "2lep,2tag2jet,75<PTV<150,SR";
   else if(cate == 52302) cate_name = "2lep,2tag3pjet,75<PTV<150,SR";
   /*
   else if(cate == 52212) cate_name = "2lep,2tag2jet,150<PTV<250,SR";
   else if(cate == 52312) cate_name = "2lep,2tag3pjet,150<PTV<250,SR";
   else if(cate == 52222) cate_name = "2lep,2tag2jet,250<PTV,SR";
   else if(cate == 52322) cate_name = "2lep,2tag3pjet,250<PTV,SR";
   */
   else if( (cate == 52212) || (cate == 52222) ) cate_name = "2lep,2tag2jet,150<PTV,SR";
   else if( (cate == 52312) || (cate == 52322) )  cate_name = "2lep,2tag3pjet,150<PTV,SR";
   else {
     Info("TranslateRecoL2()", "The stage bin (%i) is unknow, so the name will be set as UNKNOWN", cate);
     cate_name =  "UNKNOWN";
   }
   return cate_name;

}

EL::StatusCode AnalysisProcessor_STXS::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   category = 0;
   category_STXS = 0;
   // Set branch addresses and branch pointers
   if (!tree) return EL::StatusCode::FAILURE;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("event", &event, &b_event);
   fChain->SetBranchAddress("category", &category, &b_category);
   fChain->SetBranchAddress("category_STXS", &category_STXS, &b_category_STXS);
   fChain->SetBranchAddress("HTXS_Higgs_pt", &HTXS_Higgs_pt, &b_HTXS_Higgs_pt);
   fChain->SetBranchAddress("HTXS_Higgs_eta", &HTXS_Higgs_eta, &b_HTXS_Higgs_eta);
   fChain->SetBranchAddress("HTXS_Higgs_phi", &HTXS_Higgs_phi, &b_HTXS_Higgs_phi);
   fChain->SetBranchAddress("HTXS_Higgs_m", &HTXS_Higgs_m, &b_HTXS_Higgs_m);
   fChain->SetBranchAddress("HTXS_V_pt", &HTXS_V_pt, &b_HTXS_V_pt);
   fChain->SetBranchAddress("HTXS_V_eta", &HTXS_V_eta, &b_HTXS_V_eta);
   fChain->SetBranchAddress("HTXS_V_phi", &HTXS_V_phi, &b_HTXS_V_phi);
   fChain->SetBranchAddress("HTXS_V_m", &HTXS_V_m, &b_HTXS_V_m);
   fChain->SetBranchAddress("HTXS_prodMode", &HTXS_prodMode, &b_HTXS_prodMode);
   fChain->SetBranchAddress("HTXS_Stage0_Category", &HTXS_Stage0_Category, &b_HTXS_Stage0_Category);
   fChain->SetBranchAddress("HTXS_Stage1_Category_pTjet25", &HTXS_Stage1_Category_pTjet25, &b_HTXS_Stage1_Category_pTjet25);
   fChain->SetBranchAddress("HTXS_Stage1_Category_pTjet30", &HTXS_Stage1_Category_pTjet30, &b_HTXS_Stage1_Category_pTjet30);
   fChain->SetBranchAddress("HTXS_Njets_pTjet25", &HTXS_Njets_pTjet25, &b_HTXS_Njets_pTjet25);
   fChain->SetBranchAddress("HTXS_Njets_pTjet30", &HTXS_Njets_pTjet30, &b_HTXS_Njets_pTjet30);
   fChain->SetBranchAddress("mBB", &mBB, &b_mBB);
   fChain->SetBranchAddress("mva", &mva, &b_mva);
   fChain->SetBranchAddress("weight", &weight, &b_weight);

   return EL::StatusCode::SUCCESS;
}

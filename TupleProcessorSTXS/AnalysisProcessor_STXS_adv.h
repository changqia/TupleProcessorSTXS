#ifndef TupleProcessorSTXS_AnalysisProcessor_STXS_adv_H
#define TupleProcessorSTXS_AnalysisProcessor_STXS_adv_H

#include <EventLoop/Algorithm.h>

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include "CxAODTools/XSectionProvider.h"
#include "CxAODTools/ConfigStore.h"
#include "CxAODReader/HistSvc.h"
#include "CxAODReader_VHbb/AnalysisReader_VHbb.h"

// Header file for the classes stored in the TTree if any.
#include "vector"

class AnalysisProcessor_STXS_adv : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;
  long m_eventCounter; //!
  long m_eventWithNan; //!


   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           mcChannel;
   Int_t           EventNumber;
   Double_t        weight;
   vector<float>   *PDFweights; // !
   vector<float>   *QCDweights; // !
   Float_t         NominalWeight;
   Float_t         Weight0;
   Float_t         PDFWeight0;
   Float_t         alphaS_up;
   Float_t         alphaS_dn;
   Float_t         VHEW;
   Float_t         HTXS_Higgs_pt;
   Float_t         HTXS_Higgs_eta;
   Float_t         HTXS_Higgs_phi;
   Float_t         HTXS_Higgs_m;
   Float_t         HTXS_V_pt;
   Float_t         HTXS_V_eta;
   Float_t         HTXS_V_phi;
   Float_t         HTXS_V_m;
   Int_t           HTXS_prodMode;
   Int_t           HTXS_Stage0_Category;
   Int_t           HTXS_Stage1_Category_pTjet25;
   Int_t           HTXS_Stage1_Category_pTjet30;
   Int_t           HTXS_Njets_pTjet25;
   Int_t           HTXS_Njets_pTjet30;

   // List of branches
   TBranch        *b_mcChannel;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_weight;   //!
   TBranch        *b_PDFweights;   //!
   TBranch        *b_QCDweights;   //!
   TBranch        *b_NominalWeight;   //!
   TBranch        *b_Weight0;   //!
   TBranch        *b_PDFWeight0;   //!
   TBranch        *b_alphaS_up;   //!
   TBranch        *b_alphaS_dn;   //!
   TBranch        *b_VHEW;   //!
   TBranch        *b_HTXS_Higgs_pt;   //!
   TBranch        *b_HTXS_Higgs_eta;   //!
   TBranch        *b_HTXS_Higgs_phi;   //!
   TBranch        *b_HTXS_Higgs_m;   //!
   TBranch        *b_HTXS_V_pt;   //!
   TBranch        *b_HTXS_V_eta;   //!
   TBranch        *b_HTXS_V_phi;   //!
   TBranch        *b_HTXS_V_m;   //!
   TBranch        *b_HTXS_prodMode;   //!
   TBranch        *b_HTXS_Stage0_Category;   //!
   TBranch        *b_HTXS_Stage1_Category_pTjet25;   //!
   TBranch        *b_HTXS_Stage1_Category_pTjet30;   //!
   TBranch        *b_HTXS_Njets_pTjet25;   //!
   TBranch        *b_HTXS_Njets_pTjet30;   //!

protected:
   ConfigStore          *m_config;
   bool                 m_debug; // !
   int                  m_Base; // !
   HistSvc              *m_histSvc; // !
   HistNameSvc          *m_histNameSvc; // !
   XSectionProvider     *m_xSectionProvider; // !
   AnalysisReader_VHbb  *m_reader; // !

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!



  // this is a standard constructor
  AnalysisProcessor_STXS_adv ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();
  virtual EL::StatusCode Init(TTree *tree);

  void setConfig (ConfigStore *config) {
    m_config = config;
  }

  EL::StatusCode fillStage1Bin ( int stage1, float weight_inserted, TString remark = "", int STXS_Stage1_Base = -1, float ptV = -1.0 );
  EL::StatusCode FillStage1AndPTV( std::string stage1_name, float Fillweight, float EWCorrection, std::string weight_info  );

  // this is needed to distribute the algorithm to the workers
  ClassDef(AnalysisProcessor_STXS_adv, 1);
};

#endif

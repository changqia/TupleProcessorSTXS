#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include "PathResolver/PathResolver.h"

#include "TupleProcessorSTXS/AnalysisProcessor_STXS.h"


int main( int argc, char* argv[] )
{
  std::string configPath = "data/TupleProcessorSTXS/framework-tuple.cfg";
  static ConfigStore* config = ConfigStore::createStore(configPath);

  std::string sample_in = "";
  config->getif<std::string>("sample_in", sample_in);
  std::vector<std::string> samples;
  config->getif< std::vector<std::string> >("samples", samples);
  TChain chain ("physics");
  for (auto isample : samples){
    std::cout<<"name of the sample: "<<isample.c_str()<<std::endl;
    std::string sample_dir = sample_in+"/"+isample;
    std::cout<<"full location : "<<sample_dir.c_str()<<std::endl;
    //chain.Add("/gpfs/automountdir/t3/atlas/atlas0/cli/VHbb/STXS/Hbb2LepNtuples/Ntuple.Z.ZH125J_MINLO.ZH125J_MINLO_llbb.0.root");
    chain.Add(sample_dir.c_str());
  }

  EL::Job job;
  SH::SampleHandler sh;
  sh.add(SH::makeFromTChain ("sample", chain));
  job.sampleHandler (sh);
  AnalysisProcessor_STXS* alg = new AnalysisProcessor_STXS;
  Info("tupleSTXS", "add the config");
  alg->setConfig( config );
  job.algsAdd (alg);

  std::string submitDir = "submitDir";
  EL::DirectDriver driver;
  Info("tupleSTXS", "submit the job!");
  driver.submit (job, submitDir);
  return 0;
}
